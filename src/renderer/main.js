import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import VueI18n from 'vue-i18n'
import storage from '@/util/cache/localStoreCache'
import eventBus from '@/util/events/EventBus'

Vue.use(VueI18n)
Vue.use(ElementUI,{ size: 'small'})

if (!process.env.IS_WEB) 
  Vue.use(require('vue-electron'))

Vue.config.productionTip = false
Vue.prototype.$eventBus=eventBus

const i18n = new VueI18n({
  // 语言标识
  locale: storage.getConfig()===null?"zh":storage.getConfig().lang,
  //this.$i18n.locale //通过切换 locale 的值来实现语言切换
  messages: {
    'zh': require('@/i18n/zh'),   // 中文语言包
    'en': require('@/i18n/en')    // 英文语言包
  }
})


/* eslint-disable no-new */
window.$Vue=new Vue({
  components: { App },
  router,
  i18n,
  store,
  template: '<App/>'
}).$mount('#app')
