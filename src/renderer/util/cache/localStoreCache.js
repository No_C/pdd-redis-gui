import {Message} from 'element-ui'
export default{
    //添加连接
    addConnection(config){
        const connections=this.getConnections();
        if(connections[config.name]){
            Message({
                message: window.$Vue.$t('localStore.DuplicateMame'),
                type: 'error'
            });
            return false
        }
        connections[config.name]=config
        return this.setConnection(connections)
    },
    //获取连接
    getConnections (returnList = false) {
        let connections=localStorage.connections===undefined?{}:JSON.parse(localStorage.connections);
        if(returnList){
            return Object.keys(connections).map(function(i){return connections[i]})
        }else{
            return connections
        }
    },
    //编辑连接
    editConnection(config,key){
        const connections=this.getConnections();
        if(!connections[key]){
            Message({
                message: window.$Vue.$t('localStore.nameNotFount'),
                type: 'error'
            });
            return false
        }
        //删除原本数据
        delete connections[key]
        connections[config.name]=config;
        return this.setConnection(connections)
    },
    //删除连接
    delConnection(key){
        const connections=this.getConnections();
        if(!connections[key]){
            Message({
                message: window.$Vue.$t('localStore.nameNotFount'),
                type: 'error'
            });
            return false
        }
        delete connections[key]
        return this.setConnection(connections)
    },
    //设置连接
    setConnection(connections){
        //必须是键值对对象
        localStorage.connections=JSON.stringify(connections)
        return true
    },
    //设置配置
    setConfig(config){
        localStorage.pdd_redis_config=JSON.stringify(config)
    },
    //获取配置
    getConfig(){
        return localStorage.pdd_redis_config===undefined?null:JSON.parse(localStorage.pdd_redis_config)
    }
}