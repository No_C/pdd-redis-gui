import Vue from 'vue'

var eventBus=new Vue()

//eventBus
export default {
    $on(...event){
        eventBus.$on(...event)
    },
    $emit(...event){
        eventBus.$emit(...event)
    }
}