import fontList from 'font-list';

export function getFonts(){
    return fontList.getFonts().then((res)=>{
        const newFontList = [];
        res.map(item => {
            if (item.indexOf('"') === 0) {
                newFontList.push(item.replace(/^"|"$/g,''));
            } else {
                newFontList.push(item);
            }
        })
        return newFontList
    })
}