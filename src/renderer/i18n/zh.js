export const aside={
    addConnection:"新建连接",
    connectionForm:{
        tip:'使用集群模式时只需填入任意一个节点信息皆可，客户端会自动发现其他节点',
        StandAlone:"单机",
        Cluster:"集群",
        hostNotEmpty:"请输入host",
        nameNotEmpty:"请输入name",
        portNotEmpty:"请输入port",
        portFormatError:"port格式错误"
    },
    noMatchConnectionModel:"当前只支持StandAlone/Cluster两种连接方法,更多功能正在发掘中！",
    clusterDisableChoseDb:"集群模式下db切换功能不可用",
    tips:"提示",
    editConnectionTips:"重新编辑前需要先关闭此连接,您确定要继续吗？",
    delConnectionTips:"您确定要删除此连接吗？",
    determine:"确定",
    cancel:"取消",
    noData:"暂无数据",
    loadMore:"加载更多",
    UnsupportedConnection:"当前只支持StandAlone/Cluster两种连接方法,更多功能正在发掘中..",
    pleaseChoose:"请选择",
    AddKey:"添加key",
    PleaseEnter:"请输入内容",
    operating:"操作",
    save:"保存"
}
//redisClient
export const RedisOperating={
    connectionError:"连接失败,请检查服务是否正常。",
    passWordError:"密码错误,连接失败。",
    NeedPwd:"此服务已开启密码认证,请填写密码!",
    FailedRefreshSlots:"无法刷新插槽,请确保集群是否正常连接!"
}

// window.$Vue.$t('RedisOperating.passWordError')
export const localStore={
    DuplicateMame:"存在重复的连接名称,请检查",
    nameNotFount:"未知连接"
}

//公共
export const publicFeedback={
    addNewLine:"添加新行",
    saveSuccess:"保存成功",
    refushSuccess:"刷新成功",
    delSuccess:"删除成功",
    setSuccess:"设置成功",
    confirmDeletionTips:"您确定要删除 ",
    EnterKeyStartSreach:"回车键开始搜索",
    delLineTips:"您确定要删除此行数据？",
    editLine:"修改行",
    addLine:"添加行",
    KeyNotEmpty:"key不能为空",
    ValueNotEmpty:"Value不能为空",
    disableReName:"集群模式下无法修改keyName",
    notFoundKeyInfo:"未找到key的相关信息",
    keyAlreadyExists:"此KeyName已经存在,您是否要继续覆盖？",
    notSaveKeyDontModifyKeyName:"新增的keyName要保存后才能被修改",
    alreadyExistsRepeat:"已经存在keyName相同正在编辑的窗口,无法继续新增",
    autoRefushTTL:"自动刷新过期时间",
    export:"导出",
    delete:"删除",
    refush:"刷新"
}

//服务信息
export const server={
    redis_version:"redis版本",
    os:"系统",
    process_id:"进程id",
    multiplexing_api:"事件处理机制",
    redis_mode:"运行模式",
    mem_allocator:"内存分配器",
    used_memory_peak_human:"峰值",
    used_memory_human:"已分配",
    used_memory_lua:"lua占用",
    mem_fragmentation_ratio:"内存碎片率",
    connected_clients:"客户端连接数",
    total_commands_processed:"处理命令数",
    instantaneous_ops_per_sec:"QPS",
    total_net_input_bytes:"入口流量",
    total_net_output_bytes:"出口流量",
    server:"服务器",
    ram:"内存",
    status:"状态",
    Statistics:"键值统计",
    SysInfo:"系统信息",
    ClusterInfo:"集群信息",
    slotsInfo:"slots信息",
    ClusterStatus:"集群状态",
    ClusterState:"集群状态",
    cluster_slots_assigned:"已分配哈希槽",
    cluster_slots_ok:"hashSlots正常数",
    cluster_slots_pfail:"hashSlots临时错误数",
    cluster_slots_fail:"hashSlots错误数",
    cluster_known_nodes:"节点数量",
    cluster_stats_messages_sent:"已发送数据总数",
    cluster_stats_messages_received:"已接收数据总数",
    cluster_size:"master节点数量",
    cluster_current_epoch:"集群本地CurrentEpoch值",
    cluster_stats_messages_meet_received:"满足接收条件数",
    cluster_my_epoch:"当前对话的节点Epoch",
    nodeInfo:"节点信息",
    autoRefush:"自动刷新",
    moreWonderful:"更多精彩正在更新中.."
}

export const mainHeader={
    systemSetting:"系统设置",
    more:"更多",
    Language:"语言",
    fontSettings:"字体设置",
    Version:"版本信息",
    codeRepository:"代码仓库",
    checkUpdate:"检查更新",
    settingsSuccess:"配置成功",
    serverList:"服务列表",
    connectionEmpty:"请选择服务!"
}