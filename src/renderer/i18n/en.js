export const aside={
    addConnection:"New Connection",
    connectionForm:{
        tip:'When using the cluster mode, just fill in any node information, the client will automatically find other nodes',
        StandAlone:"StandAlone",
        Cluster:"Cluster",
        hostNotEmpty:"Please enter host.",
        nameNotEmpty:"Please enter name.",
        portNotEmpty:"Please enter port.",
        portFormatError:"port format error."
    },
    noMatchConnectionModel:"Currently only supports StandAlone / Cluster two connection methods, more functions are being explored!",
    clusterDisableChoseDb:"The db switch function is not available in cluster mode.",
    tips:"information",
    editConnectionTips:"This connection needs to be closed before re-editing. Are you sure you want to continue?",
    delConnectionTips:"Are you sure you want to delete this connection?",
    determine:"OK",
    cancel:"cancel",
    noData:"No data",
    loadMore:"Load More",
    UnsupportedConnection:"Currently only supports StandAlone / Cluster two connection methods, more functions are being explored ...",
    pleaseChoose:"please choose",
    AddKey:"Add Key",
    PleaseEnter:"Please enter",
    operating:"operating",
    save:"save"
}
//redisClient
export const RedisOperating={
    connectionError:"Connection failed, please check if the service is normal.",
    passWordError:"The password is incorrect and the connection fails.",
    NeedPwd:"Password authentication is enabled for this service, please fill in the password!",
    FailedRefreshSlots:"Unable to refresh the slot, please make sure the cluster is connected normally!"
}

//localStoreCache.js
export const localStore={
    DuplicateMame:"Duplicate connection name exists, please check",
    nameNotFount:"Unknown connection"
}

//公共
export const publicFeedback={
    addNewLine:"add NewLine",
    saveSuccess:"Saved successfully",
    refushSuccess:"Refresh successful",
    delSuccess:"Delete successfully",
    setSuccess:"Set successfully",
    confirmDeletionTips:"Are you sure you want to delete it ",
    EnterKeyStartSreach:"Enter key to start searching",
    delLineTips:"Are you sure you want to delete this row of data?",
    editLine:"Modify Line",
    addLine:"Add Line",
    KeyNotEmpty:"key cannot be empty",
    ValueNotEmpty:"Value cannot be empty",
    disableReName:"KeyName cannot be modified in cluster mode.",
    notFoundKeyInfo:"Key related information not found",
    keyAlreadyExists:"This KeyName already exists. Do you want to continue to overwrite it?",
    notSaveKeyDontModifyKeyName:"The new keyName must be saved before it can be modified.",
    alreadyExistsRepeat:"A window with the same keyName already exists and cannot be added",
    autoRefushTTL:"Automatic refresh expiration time",
    export:"export",
    delete:"delete",
    refush:"refush"
}
//服务信息
export const server={
    redis_version:"redis_version",
    os:"os",
    process_id:"process_id",
    multiplexing_api:"multiplexing_api",
    redis_mode:"redis_mode",
    mem_allocator:"mem_allocator",
    used_memory_peak_human:"used_memory_peak_human",
    used_memory_human:"used_memory_human",
    used_memory_lua:"used_memory_lua",
    mem_fragmentation_ratio:"mem_fragmentation_ratio",
    connected_clients:"connected_clients",
    total_commands_processed:"total_commands_processed",
    instantaneous_ops_per_sec:"instantaneous_ops_per_sec",
    total_net_input_bytes:"total_net_input_bytes",
    total_net_output_bytes:"total_net_output_bytes",
    server:"Server",
    ram:"Ram",
    status:"Status",
    Statistics:"Key-value statistics",
    SysInfo:"System Info",
    ClusterInfo:"Cluster information",
    slotsInfo:"slots information",
    ClusterStatus:"Cluster status",
    ClusterState:"cluster_state",
    cluster_slots_assigned:"cluster_slots_assigned",
    cluster_slots_ok:"cluster_slots_ok",
    cluster_slots_pfail:"cluster_slots_pfail",
    cluster_slots_fail:"cluster_slots_fail",
    cluster_known_nodes:"cluster_known_nodes",
    cluster_stats_messages_sent:"cluster_stats_messages_sent",
    cluster_stats_messages_received:"cluster_stats_messages_received",
    cluster_size:"cluster_size",
    cluster_current_epoch:"cluster_current_epoch",
    cluster_stats_messages_meet_received:"cluster_stats_messages_meet_received",
    cluster_my_epoch:"cluster_my_epoch",
    nodeInfo:"Nodes information",
    autoRefush:"Auto Refresh",
    moreWonderful:"More exciting is being updated.."
}

export const mainHeader={
    systemSetting:"settings",
    more:"more",
    Language:"Language",
    fontSettings:"Font Settings",
    Version:"Version",
    codeRepository:"Code Repository",
    checkUpdate:"Check for updates",
    settingsSuccess:"Successfully configured",
    serverList:"Service List",
    connectionEmpty:"Please select service"
}