import localStoreCache from '@/util/cache/localStoreCache.js'
export default {
    state: {
        connections:localStoreCache.getConnections(true)
    },
    getters:{
        connections:state=>{
            return state.connections
        }
    },
    mutations: {
       updateConnections(state){
            state.connections=localStoreCache.getConnections(true)
       }
    }
}