# pdd-redis-gui

> nodejs redis gui.

#### Build Setup

``` bash
# 安装依赖
npm install

# 推荐使用nmp
npm install -g cnpm --registry=https://registry.npm.taobao.org

# 必须安装的(npm安装比较慢，推荐使用cnmp)
npm install --global windows-build-tools

# 运行服务在 localhost:9080
npm run dev

# 打包成安装包
npm run build:win
npm run build:mac
npm run build:linux


```
项目详情图片：
![pdd-redis-gui](https://images.gitee.com/uploads/images/2020/0518/140510_62e6622e_2145391.png "[QUC~UGYDKWLAH6E]1WX9(D.png")
